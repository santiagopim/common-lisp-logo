;;; Variations on the yin-yang with lambdas Common Lisp logo. I love the
;;; Common Lisp yin-yang logo, so just playing with Inkscape I've
;;; revisited the logo with curved lambdas. Then generated it with
;;; cl-svg in Common Lisp.
;;;
;;; MIT No Attribution
;;; 
;;; Copyright 2021 Santiago Payà Miralta <santiagopim@gmail.com>
;;; 
;;; Permission is hereby granted, free of charge, to any person obtaining a copy of this
;;; software and associated documentation files (the "Software"), to deal in the Software
;;; without restriction, including without limitation the rights to use, copy, modify,
;;; merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
;;; permit persons to whom the Software is furnished to do so.
;;; 
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
;;; INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
;;; PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(defpackage #:common-lisp-logo
  (:use #:cl #:cl-svg))
(in-package #:common-lisp-logo)

(defun neg (n)
  "Negative to the number"
  (* -1 n))

(defstruct point
  (x 0.0 :type float :read-only t)
  (y 0.0 :type float :read-only t))

;; See documentation for the geometric parameters figure
;; https://gitlab.com/santiagopim/common-lisp-logo
(defparameter *R2* 300.0
  "External circle radius, the yin-yang radius.")
(defparameter *R1* 225.0
  "Internal circle radius, where the lambda's long trunk is drawn and the
ellipse semimajor axis length.")
(defparameter *H* 60.0
  "Length that lambda tips penetrate within the yin-yang semicircles.")
(defparameter *alpha* (/ pi 4)
  "Lambda's short trunk insertion point and ellipse semimajor axis angle.")
(defparameter *lambda-width* 32.0
  "Lambdas width.")
(defparameter *ring-width* 6.0
  "External ring width.")
(defparameter *ring-overlap* 1.0
  "External ring overlap with the yin-yang to avoid joining ghost.")
(defparameter *translation* (+ *R2* *ring-width*)
  "Translate to positive SVG coordinates.")

;; Colors, can be defined as "rgb(255, 255, 255)"
(defparameter *color-ring* "black")
(defparameter *color-ring-opacity* 1.0)
(defparameter *color-yin* "black")
(defparameter *color-yin-opacity* 1.0)
(defparameter *color-yin-lambda* "white")
(defparameter *color-yin-lambda-opacity* 1.0)
(defparameter *color-yang* "white")
(defparameter *color-yang-opacity* 1.0)
(defparameter *color-yang-lambda* "black")
(defparameter *color-yang-lambda-opacity* 1.0)

;; The code
(let* ((scene (make-svg-toplevel 'svg-1.1-toplevel
                                 :height (+ (* 2 *R2*)
                                            (* 2 *ring-width*))
                                 :width (+ (* 2 *R2*)
                                           (* 2 *ring-width*))))
       (C (make-point :y (neg (sqrt (- (expt *R1* 2) (expt *H* 2))))
                      :x *H*))
       (D (make-point :y (neg (abs (- *R2* (abs (point-y C)))))
                      :x *H*))
       (G (make-point :x (point-y C)
                      :y (point-x C)))
       (E (make-point :x (neg (* *R1* (cos *alpha*)))
                      :y (neg (* *R1* (sin *alpha*)))))
       (M (make-point :x (+ (* (point-x D) (cos *alpha*))
                            (* (point-y D) (sin *alpha*)))
                      :y (- (* (point-y D) (cos *alpha*))
                            (* (point-x D) (sin *alpha*)))))
       (b (sqrt (/ (expt (point-y M) 2)
                   (- 1 (/ (expt (point-x M) 2)
                           (expt *R1* 2))))))
       (C2 (make-point :x (neg (point-x C))
                       :y (neg (point-y C))))
       (D2 (make-point :x (neg (point-x D))
                       :y (neg (point-y D))))
       (G2 (make-point :x (neg (point-x G))
                       :y (neg (point-y G))))
       (E2 (make-point :x (neg (point-x E))
                       :y (neg (point-y E)))))
  
  ;; External ring
  (transform ((translate *translation* *translation*))
    (draw scene (:circle :cx 0.0 :cy 0.0 :r (+ *R2*
                                               (- (/ *ring-width* 2)
                                                  (/ *ring-overlap* 2))))
          :fill "none"
          :stroke *color-ring*
          :stroke-width (+ *ring-width* *ring-overlap*)
          :opacity *color-ring-opacity*))

  ;; Yin
  (transform ((translate *translation* *translation*) (rotate -45))
    (draw scene (:path :d (path
                            (move-to 0.0 *R2*)
                            (arc-to *R2* *R2*
                                    0 0 0 0.0 (neg *R2*))
                            (arc-to (/ *R2* 2)
                                    (/ *R2* 2)
                                    0 0 1 0.0 0.0)
                            (arc-to (/ *R2* 2)
                                    (/ *R2* 2)
                                    0 0 0 0.0 *R2*)))
          :fill *color-yin*
          :stroke "none"
          :stroke-width 0
          :opacity *color-yin-opacity*))
  
  ;; Yang
  (transform ((translate *translation* *translation*) (rotate -45))
    (draw scene (:path :d (path
                            (move-to 0.0 *R2*)
                            (arc-to *R2* *R2*
                                    0 0 1 0.0 (neg *R2*))
                            (arc-to (/ *R2* 2)
                                    (/ *R2* 2)
                                    0 0 1 0.0 0.0)
                            (arc-to (/ *R2* 2)
                                    (/ *R2* 2)
                                    0 0 0 0.0 *R2*)))
          :fill *color-yang*
          :stroke "none"
          :stroke-width 0
          :opacity *color-yang-opacity*))

  ;; Yang lambda
  (transform ((translate *translation* *translation*) (rotate -45))
    (make-group scene ()
      (draw* (:path :d (path
                         (move-to (point-x C) (point-y C))
                         (arc-to *R1* *R1* (* *alpha* (/ 180 pi))
                                 0 0 (point-x G) (point-y G))
                         (move-to (point-x D) (point-y D))
                         (arc-to *R1* b (* *alpha* (/ 180 pi))
                                 0 0 (point-x E) (point-y E))))
             :fill "none"
             :stroke *color-yang-lambda*
             :stroke-width *lambda-width*
             :opacity *color-yang-lambda-opacity*)
      (draw* (:circle :cx (point-x C)
                      :cy (point-y C)
                      :r (/ *lambda-width* 2))
             :fill *color-yang-lambda*
             :opacity *color-yang-lambda-opacity*)
      (draw* (:circle :cx (point-x G)
                      :cy (point-y G)
                      :r (/ *lambda-width* 2))
             :fill *color-yang-lambda*
             :opacity *color-yang-lambda-opacity*)
      (draw* (:circle :cx (point-x D)
                      :cy (point-y D)
                      :r (/ *lambda-width* 2))
             :fill *color-yang-lambda*
             :opacity *color-yang-lambda-opacity*)))

  ;; Yin lambda
  (transform ((translate *translation* *translation*) (rotate -45))
    (make-group scene ()
      (draw* (:path :d (path
                         (move-to (point-x C2) (point-y C2))
                         (arc-to *R1* *R1* (* *alpha* (/ 180 pi))
                                 0 0 (point-x G2) (point-y G2))
                         (move-to (point-x D2) (point-y D2))
                         (arc-to *R1* b (* *alpha* (/ 180 pi))
                                 0 0 (point-x E2) (point-y E2))))
             :fill "none"
             :stroke *color-yin-lambda*
             :stroke-width *lambda-width*
             :opacity *color-yin-lambda-opacity*)
      (draw* (:circle :cx (point-x C2)
                      :cy (point-y C2)
                      :r (/ *lambda-width* 2))
             :fill *color-yin-lambda*
             :opacity *color-yin-lambda-opacity*)
      (draw* (:circle :cx (point-x G2)
                      :cy (point-y G2)
                      :r (/ *lambda-width* 2))
             :fill *color-yin-lambda*
             :opacity *color-yin-lambda-opacity*)
      (draw* (:circle :cx (point-x D2)
                      :cy (point-y D2)
                      :r (/ *lambda-width* 2))
             :fill *color-yin-lambda*
             :opacity *color-yin-lambda-opacity*)))

  ;; Save to file
  (with-open-file (s #p"common-lisp-yinyang-coded.svg"
                     :direction :output
                     :if-exists :supersede)
    (stream-out s scene)))
