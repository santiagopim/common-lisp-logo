;;; Variations on the yin-yang with lambdas Common Lisp logo. I love the
;;; Common Lisp yin-yang logo, so just playing with Inkscape I've
;;; revisited the logo with curved lambdas. Then generated it with
;;; cl-svg in Common Lisp.
;;;
;;; MIT No Attribution
;;; 
;;; Copyright 2022 Santiago Payà Miralta <santiagopim@gmail.com>
;;; 
;;; Permission is hereby granted, free of charge, to any person obtaining a copy of this
;;; software and associated documentation files (the "Software"), to deal in the Software
;;; without restriction, including without limitation the rights to use, copy, modify,
;;; merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
;;; permit persons to whom the Software is furnished to do so.
;;; 
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
;;; INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
;;; PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(defpackage #:common-lisp-legacy-logo
  (:use #:cl #:cl-svg))
(in-package #:common-lisp-legacy-logo)

;; Auxiliar amenities
(defun neg (n)
  "Negative to the number"
  (* -1 n))

(defstruct point
  (x 0.0 :type float :read-only t)
  (y 0.0 :type float :read-only t))

;; Colors
(defparameter *color-ring* "black")
(defparameter *color-yin* "black")
(defparameter *color-yang* "white")
(defparameter *color-yin-lambda* "white")
(defparameter *color-yang-lambda* "black")
(defparameter *color-lambda-stroke* "black")
(defparameter *width-lambda-stroke* 0.0)

;; General dimensions
(defparameter *R2* 300.0
  "The yin-yang radius.")
(defparameter *circle-width* 8.0
  "External circle width.")
(defparameter *translation* (+ *R2* *circle-width*)
  "Translate to positive SVG coordinates, because external circle.")

;;; From https://upload.wikimedia.org/wikipedia/commons/4/48/Lisp_logo.svg
;;; The logo is 470 pixels wide. The lambda's path has this coordinates:
;;;
;;; <path
;;;   d="m 272,89.72
;;;      c 78.19,85.94 24.93,152.64 -24,250
;;;      h 40
;;;      c 22.05,-35.18 39.35,-75.10 55.25,-116.43
;;;        15.55,37.01 30.81,74.28 40.75,116.43
;;;      h 40
;;;      c -29.56,-102.33 -64.43,-172.76 -112,-250
;;;      z"
;;;  id="path9654"
;;;  style="display:inline" />
;;;
;;; So the values for our lambda uses these coordinates, transformed with
;;; the `normalize' function because de different dimensions. Though about
;;; two cases in which it is possible to do without the function `normalize':
;;;
;;;   - Set *R2* a constant with value 235, the same radius that in the
;;;     wikipedia template. So it is not necessary the normalize function.
;;;
;;;   - Use directly the template's coordinates and adjust at the end with
;;;     scale transformation the whole lambda.

(defun normalize (p)
  "Translate coordinates from template."
  (* (/ p 235) *R2*))

;;; See documentation for the geometric parameters figure
;;; https://gitlab.com/santiagopim/common-lisp-logo

;; The code
(let* ((scene (make-svg-toplevel 'svg-1.1-toplevel
                                 :height (+ (* 2 *R2*)
                                            (* 2 *circle-width*))
                                 :width (+ (* 2 *R2*)
                                           (* 2 *circle-width*))))
       (L1 (make-point :x (normalize (- 272 235)) 
                       :y (neg (normalize (- 235 89.72)))))
       (C11 (make-point :x (+ (point-x L1) (normalize 78.19))
                        :y (+ (point-y L1) (normalize 85.94))))
       (C12 (make-point :x (+ (point-x L1) (normalize 24.93))
                        :y (+ (point-y L1) (normalize 152.64))))
       (L2 (make-point :x (+ (point-x L1) (normalize -24))
                       :y (+ (point-y L1) (normalize 250))))
       (L3 (make-point :x (+ (point-x L2) (normalize 40))
                       :y (point-y L2)))
       (C21 (make-point :x (+ (point-x L3) (normalize 22.05))
                        :y (+ (point-y L3) (normalize -35.18))))
       (C22 (make-point :x (+ (point-x L3) (normalize 39.35))
                        :y (+ (point-y L3) (normalize -75.10))))
       (L4 (make-point :x (+ (point-x L3) (normalize 55.25))
                       :y (+ (point-y L3) (normalize -116.43))))
       (C31 (make-point :x (+ (point-x L4) (normalize 15.55))
                        :y (+ (point-y L4) (normalize 37.01))))
       (C32 (make-point :x (+ (point-x L4) (normalize 30.81))
                        :y (+ (point-y L4) (normalize 74.28))))
       (L5 (make-point :x (+ (point-x L4) (normalize 40.75))
                       :y (+ (point-y L4) (normalize 116.43))))
       (L6 (make-point :x (+ (point-x L5) (normalize 40))
                       :y (point-y L5)))
       (C41 (make-point :x (+ (point-x L6) (normalize -29.56))
                        :y (+ (point-y L6) (normalize -102.33))))
       (C42 (make-point :x (+ (point-x L6) (normalize -64.43))
                        :y (+ (point-y L6) (normalize -172.76))))
       (L7 (make-point :x (+ (point-x L6) (normalize -112))
                       :y (+ (point-y L6) (normalize -250)))))

  ;; External circle
  (transform ((translate *translation* *translation*))
    (draw scene (:circle :cx 0.0 :cy 0.0 :r (+ *R2* (/ *circle-width* 2)))
          :fill "none" :stroke *color-ring* :stroke-width *circle-width*))

  ;; Yin
  (transform ((translate *translation* *translation*) (rotate -45))
    (draw scene (:path :d (path
                            (move-to 0.0 *R2*)
                            (arc-to *R2* *R2*
                                    0 0 0 0.0 (neg *R2*))
                            (arc-to (/ *R2* 2)
                                    (/ *R2* 2)
                                    0 0 1 0.0 0.0)
                            (arc-to (/ *R2* 2)
                                    (/ *R2* 2)
                                    0 0 0 0.0 *R2*)))
          :fill *color-yin*
          :stroke "none"
          :stroke-width 0))
  
  ;; Yang
  (transform ((translate *translation* *translation*) (rotate -45))
    (draw scene (:path :d (path
                            (move-to 0.0 *R2*)
                            (arc-to *R2* *R2*
                                    0 0 1 0.0 (neg *R2*))
                            (arc-to (/ *R2* 2)
                                    (/ *R2* 2)
                                    0 0 1 0.0 0.0)
                            (arc-to (/ *R2* 2)
                                    (/ *R2* 2)
                                    0 0 0 0.0 *R2*)))
          :fill *color-yang*
          :stroke "none"
          :stroke-width 0))

  ;; Lambda
  (defun make-lambda (color)
    (make-group scene ()
      (draw* (:path :d (path
                         (move-to (point-x L1) (point-y L1))
                         (curve-to (point-x C11) (point-y C11)
                                   (point-x C12) (point-y C12)
                                   (point-x L2) (point-y L2))
                         (horizontal-to (point-x L3))
                         (curve-to (point-x C21) (point-y C21)
                                   (point-x C22) (point-y C22)
                                   (point-x L4) (point-y L4))
                         (curve-to (point-x C31) (point-y C31)
                                   (point-x C32) (point-y C32)
                                   (point-x L5) (point-y L5))
                         (horizontal-to (point-x L6))
                         (curve-to (point-x C41) (point-y C41)
                                   (point-x C42) (point-y C42)
                                   (point-x L7) (point-y L7))
                         (close-path)))
             :fill color
             :stroke *color-lambda-stroke*
             :stroke-width *width-lambda-stroke*)))
    
  ;; Yin lambda
  (transform ((translate *translation* *translation*))
    (make-lambda *color-yin-lambda*))

  ;; Yang lambda
  (transform ((translate *translation* *translation*) (rotate 180))
    (make-lambda *color-yang-lambda*))
  
  ;; Save to file
  (with-open-file (s #p"common-lisp-yinyang-legacy-coded.svg"
                     :direction :output
                     :if-exists :supersede)
    (stream-out s scene)))
